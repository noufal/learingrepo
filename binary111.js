var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
console.log(binaryS(arr, 10));

function binaryS(arr, value) {

    var low = 0;
    var high = arr.length - 1;
    while (low < high) {
        var guss = Math.floor((low + high) / 2);
        //console.log('low',low, 'high', high);
        if (arr[guss] == value) {
            return arr[guss];
        }
        else if (arr[guss] > value) {
            high = guss - 1;
        }
        else{
            low=guss+1;
        }
    }
    return false;
}